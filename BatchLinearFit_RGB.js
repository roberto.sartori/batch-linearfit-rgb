
/*
 * FileList
 *
 * Recursively search a directory tree for all existing files with the
 * specified file extensions.
 */
function FileList( dirPath, extensions, verbose )
{
   /*
    * Regenerate this file list for the specified base directory and file
    * extensions.
    */
   this.regenerate = function( dirPath, extensions, verbose )
   {
      // Security check: Do not allow climbing up a directory tree.
      if ( dirPath.indexOf( ".." ) >= 0 )
         throw new Error( "FileList: Attempt to redirect outside the base directory: " + dirPath );

      // The base directory is the root of our search tree.
      this.baseDirectory = File.fullPath( dirPath );
      if ( this.baseDirectory.length == 0 )
         throw new Error( "FileList: No base directory has been specified." );

      // The specified directory can optionally end with a separator.
      if ( this.baseDirectory[ this.baseDirectory.length - 1 ] == '/' )
         this.baseDirectory.slice( this.baseDirectory.length - 1, -1 );

      // Security check: Do not try to search on a nonexisting directory.
      if ( !File.directoryExists( this.baseDirectory ) )
         throw new Error( "FileList: Attempt to search a nonexistent directory: " + this.baseDirectory );

      // If no extensions have been specified we'll look for all existing files.
      if ( extensions == undefined || extensions == null || extensions.length == 0 )
         extensions = [ '' ];

      if ( verbose )
      {
         console.writeln( "<end><cbr><br>==> Finding files from base directory:" );
         console.writeln( this.baseDirectory );
      }

      // Find all files with the required extensions in our base tree recursively.
      this.files = [];
      for ( let i = 0; i < extensions.length; ++i )
         this.files = this.files.concat( searchDirectory( this.baseDirectory + "/*" + extensions[ i ], true /*recursive*/ ) );

   };

   this.baseDirectory = "";
   this.files = [];
   this.index = [];

   if ( dirPath != undefined )
      this.regenerate( dirPath, extensions, verbose );

   if ( verbose )
   {
      console.writeln( "<end><cbr>" + this.files.length + " file(s) found:" );
      for ( let i = 0; i < this.files.length; ++i )
         console.writeln( this.files[ i ] );
   }
}

FileList.prototype = new Object;

function getFiles() {
   let gdd = new GetDirectoryDialog;
   let files = [];

   gdd.caption = "Select your directory";
   if ( gdd.execute() )
   {
      let rootDir = gdd.directory;

      // get the list of compatible file extensions
      let openFileSupport = new OpenFileDialog;
      openFileSupport.loadImageFilters();
      let filters = openFileSupport.filters[ 0 ]; // all known format
      filters.shift();
      filters = filters.concat( filters.map( f => ( f.toUpperCase() ) ) );

      // perform the search
      let filesFound = 0;
      let addedFiles = 0;
      let L = new FileList( rootDir, filters, false /*verbose*/ );
      L.files.forEach( filePath =>
      {
         filesFound++;
         files.push( filePath );
      } );
   }

   return files;
}

function main() {
   let files = getFiles();
   console.noteln(files);

   var CE = new ChannelExtraction;
   CE.colorSpace = ChannelExtraction.prototype.RGB;
   CE.sampleFormat = ChannelExtraction.prototype.SameAsSource;

   var CC = new ChannelCombination;
   CC.colorSpace = ChannelCombination.prototype.RGB;

   var LF = new LinearFit;
   LF.rejectLow = 0.000000;
   LF.rejectHigh = 0.920000;

   for (let i=0; i<files.length; i++) {
      // open the file
      let fname = files[i];
      console.noteln("opening ", fname);
      let workingWindow = ImageWindow.open(fname)[0];

      // split the RGB channels
      CE.channels = [ // enabled, id
         [true, ""],
         [true, ""],
         [true, ""]
      ];
      CE.executeOn(workingWindow.mainView);

      let rID = workingWindow.mainView.id + "_R";
      let gID = workingWindow.mainView.id + "_G";
      let bID = workingWindow.mainView.id + "_B";

      // Linear Fit the G channel
      LF.referenceViewId = gID;
      LF.executeOn(ImageWindow.windowById(rID).mainView);
      LF.executeOn(ImageWindow.windowById(bID).mainView);

      // Recombine
      CC.channels = [ // enabled, id
      [true, rID],
      [true, gID],
      [true, bID]
      ];
      CC.executeGlobal();
      let combined = ImageWindow.activeWindow;

      // Save the new file
      let ext= File.extractExtension(fname);
      let targetName = fname.replace(ext, "_ln"+ext);
      combined.saveAs(targetName, false, false, true, false);

      // close the working images
      workingWindow.forceClose();
      ImageWindow.windowById(rID).forceClose();
      ImageWindow.windowById(gID).forceClose();
      ImageWindow.windowById(bID).forceClose();
      combined.forceClose();

   }
}

main();
